<?php

namespace Drupal\views_ticker_type_text\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure site-wide settings for Views Ticker Type Text Style Plugin.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'views_ticker_type_text_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['views_ticker_type_text.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('views_ticker_type_text.settings');

    $form['library_location_ttt'] = [
      '#type' => 'radios',
      '#title' => $this->t('Ticker Type Text library location/version'),
      '#description' => $this->t('If serving the files from a local path, the library MUST be located in libraries/ticker-type-text.  See the module README file for more information.'),
      '#options' => [
        'cdn' => $this->t('CDN - latest version (not recommended)'),
        'cdn_2.0.5' => $this->t('CDN - version 2.0.5'),
        'local' => $this->t('Local path (libraries/ticker-type-text)'),
      ],
      '#default_value' => $config->get('library_location_ttt') ?? 'cdn_2.0.5',
    ];

    $form['library_location_visable'] = [
      '#type' => 'radios',
      '#title' => $this->t('Visable library location/version'),
      '#description' => $this->t('If serving the files from a local path, the library MUST be located in libraries/visable.  See the module README file for more information.'),
      '#options' => [
        'cdn' => $this->t('CDN - latest version (not recommended)'),
        'cdn_2.0.2' => $this->t('CDN - version 2.0.2'),
        'local' => $this->t('Local path (libraries/visable)'),
      ],
      '#default_value' => $config->get('library_location_visable') ?? 'cdn_2.0.2',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('views_ticker_type_text.settings')
      ->set('library_location_ttt', $values['library_location_ttt'])
      ->set('library_location_visable', $values['library_location_visable'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
