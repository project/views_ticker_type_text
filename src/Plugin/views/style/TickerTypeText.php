<?php

namespace Drupal\views_ticker_type_text\Plugin\views\style;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render list of fields or items as Ticker Type Text slides  .
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "ticker_type_text",
 *   title = @Translation("Ticker Type Text"),
 *   help = @Translation("Display the results in a Ticker Type Text View."),
 *   theme = "views_ticker_type_text_view_ticker_type_text",
 *   display_types = {"normal"}
 * )
 */
class TickerTypeText extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * Constructs a TickerTypeText object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ImmutableConfig $module_configuration
   *   The Views Ticker Type Text module's configuration.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $module_configuration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configuration['library_location_ttt'] = $module_configuration->get('library_location_ttt');
    $this->configuration['library_location_visable'] = $module_configuration->get('library_location_visable');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $module_configuration = $container->get('config.factory')->get('views_ticker_type_text.settings');
    return new static($configuration, $plugin_id, $plugin_definition, $module_configuration);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['type'] = ['default' => 'ul'];
    $options['type'] = ['default' => 'ul'];
    $options['class'] = ['default' => 'ttt_items'];
    $options['wrapper_class'] = ['default' => 'ttt_container'];
    $options['row_class'] = ['default' => 'ttt_items'];
    $options['item_class'] = ['default' => 'ttt_item'];
    $options['headline_class'] = ['default' => 'ttt'];
    $options['ticker_type_text_config'] = [
      // 'default' => ".ttt",
      'contains' => [
        'keep' => ['default' => 0],
        'seconds' => ['default' => 10],
        'delay' => ['default' => 20],
        'iterations' => ['default' => 0],
        'ratio' => ['default' => 0.9],
        'secondsout' => ['default' => 0],
        'dev_mode' => ['default' => FALSE],
        'pausetarget' => ['default' => FALSE],
        'stoptarget' => ['default' => FALSE],
      ],
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $initial_labels = ['' => $this->t('- None -')];
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);
    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('List type'),
      '#options' => [
        'ul' => $this->t('Unordered list'),
        'ol' => $this->t('Ordered list'),
      ],
      '#default_value' => $this->options['type'],
    ];
    $form['wrapper_class'] = [
      '#title' => $this->t('Wrapper class'),
      '#description' => $this->t('The class to provide on the wrapper, outside the list.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['wrapper_class'],
    ];
    $form['class'] = [
      '#title' => $this->t('List class'),
      '#description' => $this->t('The class to provide on the list element itself.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['class'],
    ];
    $form['item_class'] = [
      '#title' => $this->t('Item class'),
      '#description' => $this->t('The class to provide on each row.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['item_class'],
    ];
    $form['headline_class'] = [
      '#title' => $this->t('Headline class'),
      '#description' => $this->t('The class to provide for the Headline.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['headline_class'],
    ];
    // Ticker Type Text general configuration. Values within this fieldset
    // will be passed directly to the Ticker Type Text settings object.
    // As a result, form element keys should be given the same ID as TTT
    // parameter, e.g. $form['ticker_type_text_config']['id_of_ttt_parameter'].
    // See the list of parameters at
    // https://github.com/2dareis2do/ticker-type-text#parameters.
    $form['ticker_type_text_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Text Type Ticker Options'),
      '#description' => $this->t('Each of these settings maps directly to one of the TimelineJS presentation options.  See the <a href="@options-doc">options documentation page</a> for additional information.', ['@options-doc' => 'https://timeline.knightlab.com/docs/options.html']),
      '#open' => TRUE,
    ];
    $form['ticker_type_text_config']['keep'] = [
      '#type' => 'number',
      '#title' => $this->t('Keep'),
      '#description' => $this->t('keep denotes the number characters to keep when transitioning out'),
      '#default_value' => $this->options['ticker_type_text_config']['keep'],
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#required' => TRUE,
    ];
    $form['ticker_type_text_config']['seconds'] = [
      '#type' => 'number',
      '#title' => $this->t('Seconds'),
      '#description' => $this->t('Number of seconds it takes to iterate through slide'),
      '#default_value' => $this->options['ticker_type_text_config']['seconds'],
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#required' => TRUE,
    ];
    $form['ticker_type_text_config']['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay'),
      '#description' => $this->t('Delay number of milliseconds delay between hiding and showing each character'),
      '#default_value' => $this->options['ticker_type_text_config']['delay'],
      '#min' => 0,
      '#max' => 1000,
      '#step' => 10,
      '#required' => TRUE,
    ];
    $form['ticker_type_text_config']['iterations'] = [
      '#type' => 'number',
      '#title' => $this->t('Iterations'),
      '#description' => $this->t('Iterations for the text ticker to cycle through all elements'),
      '#default_value' => $this->options['ticker_type_text_config']['iterations'],
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#required' => TRUE,
    ];
    $form['ticker_type_text_config']['ratio'] = [
      '#type' => 'range',
      '#title' => $this->t('Ratio'),
      '#description' => $this->t('ratio used for setting both when transition in and out starts'),
      '#default_value' => $this->options['ticker_type_text_config']['ratio'],
      '#min' => 0,
      '#max' => 1,
      '#step' => 'any',
      '#step' => 'any',
      '#required' => TRUE,
    ];
    $form['ticker_type_text_config']['secondsout'] = [
      '#type' => 'number',
      '#title' => $this->t('Seconds out'),
      '#description' => $this->t('secondsout so we can set the the speed of the second part if using keep'),
      '#default_value' => $this->options['ticker_type_text_config']['secondsout'],
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#required' => TRUE,
    ];
    $form['ticker_type_text_config']['dev_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dev mode'),
      '#description' => $this->t('dev mode boolean (false by default).'),
      '#default_value' => $this->options['ticker_type_text_config']['dev_mode'],
    ];
    $form['ticker_type_text_config']['pausetarget'] = [
      '#type' => 'textfield',
      '#title' => $this->t('pausetarget element'),
      '#description' => $this->t('pausetarget element to target for trigger e.g. input button'),
      '#default_value' => $this->options['ticker_type_text_config']['pausetarget'],
      '#size' => 60,
      '#maxlength' => 128,
    ];
    $form['ticker_type_text_config']['stoptarget'] = [
      '#type' => 'textfield',
      '#title' => $this->t('stoptarget element'),
      '#description' => $this->t('stoptarget element to target for trigger e.g. input button'),
      '#default_value' => $this->options['ticker_type_text_config']['stoptarget'],
      '#size' => 60,
      '#maxlength' => 128,
    ];
    // Field mapping.
    $form['ticker_type_text_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Field mappings'),
      '#description' => $this->t('Map your Views data fields to TimelineJS slide object properties.'),
      '#open' => TRUE,
    ];
    $form['ticker_type_text_fields']['headline'] = [
      '#type' => 'select',
      '#options' => $view_fields_labels,
      '#title' => $this->t('Headline'),
      '#description' => $this->t('The selected field may contain any text, including HTML markup.'),
      '#default_value' => $this->options['ticker_type_text_fields']['headline'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    // Return if the start date field mapping is not configured.
    if (empty($this->options['ticker_type_text_fields']['headline'])) {
      $this->messenger()->addWarning(t('The Headline field mapping must be configured in the TickerTypeText format settings before any slides can be rendered.'));
      return;
    }

    // Get headline field mapping.
    $selected_headline_field = $this->options["ticker_type_text_fields"]["headline"];

    $rows = [];
    // Render.
    $this->renderFields($this->view->result);
    // Iterate.
    foreach ($this->view->result as $i => $row) {
      $row->selected_markup_title = $this->rendered_fields[$i][$selected_headline_field];
      $rows[] = $row;
    }

    return [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#rows' => $rows,
    ];
  }

}
