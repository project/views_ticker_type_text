Views Ticker Type Text 
================
This module provides a Views Style Plugin to enable you to use a Ticker Type 
Effect with any custom View when using the drupal/views module.

Installation
------------
Download the module using Composer with the command ```composer require
drupal/views_ticker_type_text``` and enable it.  By default, there are no 
library files to download because they are served from the CDN. New
installations will be configured to use the last version with which the module 
was tested. You can change this setting at
```admin/config/development/views_ticker_type_text```.  Other options include 
using the latest version from the CDN or to serve the library files locally from
your own server.

> **_UPDATE 24/03/2024:_**
> Library dependencies should now be downloaded by default by Composer. This
> assumes drupal library is in the `web` folder e.g. `/web/libraries`

~~If you want to serve the library files from your own site instead of the CDN
and use composer to manage your dependencies, I would recommend using excellent
`oomphinc/composer-installers-extender`. I have written an short article on how
this can be used to install your js dependencies by type here:
`https://www.danlobo.co.uk/article/using-composer-manage-your-js-dependencies`~~

Alternatively, you can download and install them locally. ZIP files of the
compiled library are available on github and elsewhere. You may download a
specific version from github:
- https://github.com/2dareis2do/vis
- https://github.com/2dareis2do/ticker-type-text

Ultimately, for this to function correctly you will need to have the 
following files in these directories:

1. ```/libraries/ticker-type-text/dist/js/index.js```
2. ```/libraries/ticker-type-text/dist/css/style.css```
3. ```/libraries/visable/dist/index.js```

Configuring the Plugin
----------------------
The 'views_ticker_type_text' views style plugin provides multiple options that 
can easily be configured from the 'Settings' form found in the Ticker Type Text 
View in the Format section. 

For more details on these parameters, please see the included 
documentation at https://github.com/2dareis2do/ticker-type-text

To get started you will need to select the field that you want to use and map it
to the headline field. Typically this could be a node or entity title with a
link etc as you may well have seen. The plugin should also allow you to use 
views \tokens and other options to rewite the markup to suit your needs.

### CSS
To hide the items that the Ticker Type Text plugin uses, you may well need to 
add some css to your front end theme. e.g. 

```
.ttt_items {
  display: none;
}
```

Author
-----------
* Daniel Lobo (2dareis2do)
