/**
 * @file
 * Invokes the Ticker Type text library for each list of itemms in a view  .
 */

(function ($, once, Drupal, drupalSettings) {
  "use strict";
  /**
   * Handle the ticker type text plugin on page load.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Initialize infinite scroll pagers and bind the scroll event.
   * @prop {Drupal~behaviorDetach} detach
   *   During `unload` remove the scroll event binding.
   */
  Drupal.behaviors.TickerTypeText = {
    attach: function (context, settings) {
      once('tickerTypeText', 'html', context).forEach(function (element) {
  
          const keep = +drupalSettings.TickerTypeText.options.keep;
          const seconds = +drupalSettings.TickerTypeText.options.seconds;
          const delay = +drupalSettings.TickerTypeText.options.delay;
          const iterations = +drupalSettings.TickerTypeText.options.iterations;
          const ratio = +drupalSettings.TickerTypeText.options.ratio;
          const secondsout = +drupalSettings.TickerTypeText.options.secondsout;
          const dev_mode = drupalSettings.TickerTypeText.options.dev_mode;
          const pausetarget = drupalSettings.TickerTypeText.options.pausetarget;
          const stoptarget = drupalSettings.TickerTypeText.options.stoptarget;

          let $elements = $(element).find(".ttt_container .ttt_item");
          let $target = $(element).find(".ttt_container .ttt");

          if ($elements.length > 0 && $target.length > 0) {
            let animatetext = function () {
              // $target.tickerText($elements, 0, 10, 20, 0, 0.9, 0, false, false, false);
              $target.tickerText($elements, keep, seconds, delay, iterations, ratio, secondsout, dev_mode, pausetarget, stoptarget);
            };
            $(animatetext);
          }
        })
    
    },
    detach: function (context, settings, trigger) {
      // In the case where the view is removed from the document, remove it's
      // events.
      if (trigger === 'unload') {
        once.remove('tickerTypeText', 'html', context)
      }
    }
  };

 
})(jQuery, once, Drupal, drupalSettings);

